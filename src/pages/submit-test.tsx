import React,{useState} from 'react'
import { Grid, TextField, Button, Paper, Checkbox } from '@material-ui/core';
import axios from 'axios';
import { withStyles } from "@material-ui/core/styles";

const styles = (theme) => ({
  alertSuccess: {
    color: '#3c763d',
    backgroundColor: '#dff0d8',
    borderColor: '#d6e9c6',
    padding: '20px'
  },
  alertDanger: {
    color: '#a94442',
    backgroundColor: '#f2dede',
    borderColor: '#ebccd1',
    padding: '20px'
  },
});

function SubmitTest(props) {
  const { classes } = props;
  const [testCases, setTestCases] = useState([{ name: '', classes: [''] }]);
  const [responseText, setResponseText] = useState({status:'',text:''});
  const [showLoader, setShowLoader] = useState(false);
  const [parallel, setParallel]= useState(false);

  const addNewTestCase = () => {
    let testCasesList = [...testCases];
    testCasesList.push({name:'',classes:['']});
    setTestCases(testCasesList)
  }

  const handleAddClass = (testCase,index) => {
     let testCasesList = [...testCases];
     testCasesList[index].classes.push("");
     setTestCases(testCasesList);
  };

  const handleClassName = (e, testCaseIndex, classIndex) => {
    let { name, value } = e.target;
    let testCasesList = [...testCases];
    let reqClasses = testCasesList[testCaseIndex].classes;
    reqClasses[classIndex] = value;
    setTestCases(testCasesList);
  }

  const handleTestCaseName = (e, testCaseIndex) => {
    let { name, value } = e.target;
    let testCasesList = [...testCases];
    let reqTestCase = testCasesList[testCaseIndex];
    reqTestCase['name'] = value;
    setTestCases(testCasesList);
  }

  const handleRemoveTestCase = (i) => {
    let testCasesList = [...testCases];
    testCasesList.splice(i,1);
    setTestCases(testCasesList);
  }

  const handleRemoveClass = (classIndex,testCaseIndex) => {
    let testCasesList = [...testCases];
    let reqClasses = testCasesList[testCaseIndex].classes;
    reqClasses.splice(classIndex,1)
    setTestCases(testCasesList);
  }

  const submitTest = () => {
    let reqData = new Object();
    reqData['testSuiteId'] = '0003';
    reqData['parallel'] = parallel;
    reqData['tests'] = testCases;
    setResponseText({status:'',text:''});
    setShowLoader(true);
    axios({
     method: "post",
     url: "http://localhost:8080/quat/api/v1/aws/s3/upload/xml",
     data: reqData,
   }).then(res => {
      setShowLoader(false);
      if(res.data) {
        setResponseText({ status: 'success', text: res.data });
      }
     }).catch((err)=>{
       setShowLoader(false);
       setResponseText({ status: 'failed', text: JSON.stringify(reqData) });
     });
 }

  const getClasses = (testCase,index) => {
  return testCase.classes.map((x, i) => {
      return (
        <Grid container style={{ marginBottom: '19px' }}>
            <Grid item xs={4}>
              <TextField id="standard-basic" label="Class name" value={x} onChange={e => handleClassName(e, index,i)} />
            </Grid>
          <Grid item xs={8} style={{marginTop:'17px'}}>
           <Grid container spacing={2}>
                {testCase.classes.length > 1 &&
                  <Grid item>
                    <Button
                      onClick={() => handleRemoveClass(i, index)}
                      color="default"
                      size="small"
                    variant="contained">
                      -
                      </Button>
                  </Grid>
                }
              <Grid item>
                  {testCase.classes.length - 1 === i &&
                    <Button
                      onClick={() => handleAddClass(x, index)}
                      color="default"
                      size="small"
                    variant="contained">
                    +
                      </Button>
                  }
              </Grid>
            </Grid>
          </Grid>
      </Grid>
      );
    });
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
      <h2>Create Test</h2>
        <form noValidate autoComplete="off">
          {testCases.map((testCase, i) => {
            return (
              <Paper elevation={3} style={{padding:'12px',marginBottom:'10px'}}>
                {/* <b>Test Case{i+1}</b> */}
                <div style={{ marginBottom: '19px',marginTop:'19px' }}>
                  <TextField id="standard-basic" label="Test Case Name" onChange={(e)=>handleTestCaseName(e,i)}   />
                </div>
                {getClasses(testCase,i)}
              <Grid container spacing={2}>
                {testCases.length > 1 && <Grid item><Button color="secondary" variant="outlined" onClick={()=>handleRemoveTestCase(i)}>Remove Testcase</Button></Grid>}
                <Grid item>
                {testCases.length-1 === i && (<Button color="primary" variant="outlined" onClick={addNewTestCase}>Add Testcase</Button>)}
                </Grid>
              </Grid>  
              </Paper>             
          )
          })}
        </form>
      </Grid>
      <Grid item xs={12}>
        <Checkbox
          onChange={() => setParallel(!parallel)}
          value={parallel}
        color="primary"
        inputProps={{ 'aria-label': 'secondary checkbox' }}
      />
          Check parallel
      </Grid>
      <Grid item xs={12}>
        <Button color='primary' variant='contained' onClick={submitTest}>{showLoader ? 'Loading...' : 'Submit Test'}</Button>
        {responseText.text ? responseText.status == 'success' ?
          <p className={classes.alertSuccess}>{responseText.text}</p> :
          <p className={classes.alertDanger}>{responseText.text}</p>
          : ''}
      </Grid>
      <Grid item xs={12}>
        {JSON.stringify(testCases)}
      </Grid>
    </Grid>
  )
}
export default withStyles(styles)(SubmitTest);
