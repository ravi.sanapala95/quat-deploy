import * as React from 'react';
import { DataGrid, ColDef, ValueGetterParams } from '@material-ui/data-grid';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Grid, TextField, Paper, Checkbox } from '@material-ui/core';
import Button from '@material-ui/core/Button';

const columns: ColDef[] = [
  { field: 'id', headerName: 'Test SuiteID', width: 180 },
  { field: 'firstName', headerName: 'Created by', width: 130 },
  { field: 'lastName', headerName: 'Created on', width: 130 },
  {
    field: 'age',
    headerName: 'No of test cases',
    type: 'number',
    width: 190,
  },
  {
    field: 'fullName',
    headerName: 'Status',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160,
    valueGetter: (params: ValueGetterParams) =>
      `Passed`,
  },
];
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }),
);
const rows = [
  { id: 'WAVE1', lastName: '12-02-2021', firstName: 'Jon', age: 35 },
  { id: 'WAVE2', lastName: '12-02-2021', firstName: 'Cersei', age: 42 },
  { id: 'WAVE3', lastName: '12-02-2021', firstName: 'Jaime', age: 45 },
  { id: 'WAVE4', lastName: '12-02-2021', firstName: 'Arya', age: 16 },
  { id: 'WAVE5', lastName: '12-02-2021', firstName: 'Daenerys', age: null },
  { id: 'WAVE6', lastName: '12-02-2021', firstName: null, age: 150 },
  { id: 'WAVE7', lastName: '12-02-2021', firstName: 'Ferrara', age: 44 },
  { id: 'WAVE8', lastName: '12-02-2021', firstName: 'Rossini', age: 36 },
  { id: 'WAVE9', lastName: '12-02-2021', firstName: 'Harvey', age: 65 },
];

export default function DataTable() {
    const classes = useStyles();
  return (
      <>
    <h1>Test Suites Available</h1>
    <div style={{ height: 400, width: '100%' }}>
    <DataGrid rows={rows} columns={columns} pageSize={5} checkboxSelection />
    </div>
    <div className={classes.root}>
    <Grid container >
    <Grid item xs={8}></Grid>
    <Grid item xs={2} style={{textAlign:"right"}}>
        <Button variant="contained" color="secondary">
        Stop
      </Button>
      </Grid>
      <Grid item xs={2} style={{textAlign:"right"}}> <Button variant="contained" color="primary" >
        Run
      </Button>
      </Grid>
   
    </Grid>
      
        </div>
      </>

  );
}