import React from 'react';
import { AppProps } from 'next/app';
import MyAppLayout from '../components/layouts/Layout'
import '../styles/globals.css'
import { useRouter } from 'next/router';
import {useState} from 'react';
// external css
import 'react-toastify/dist/ReactToastify.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'react-flexbox-grid/dist/react-flexbox-grid.css';
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css';


export default function CustomApp({ Component, pageProps }: AppProps) {
 const [path, setPath] = useState(false);
  React.useEffect(() => {
  }, []);
  const { asPath } = useRouter()
   if(asPath === '/login'){
      return (
        <Component {...pageProps} />
      );
   }
   else
   return(<MyAppLayout><Component {...pageProps} /></MyAppLayout>)
 
}
