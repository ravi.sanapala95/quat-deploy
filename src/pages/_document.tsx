import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document';
export default class CustomDocument extends Document<any> {
  render() {
    return (
      <Html lang="en-US">
        <Head>
          <meta name="theme-color" content="#000000" />
          <meta
            name="Description"
            content="Inst is a GraphQL based server side dashboard template"
          />
        </Head>
        <body dir="ltr">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
