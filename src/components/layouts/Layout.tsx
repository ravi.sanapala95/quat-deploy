// import Nav from './Nav'
// import Meta from './Meta'
import React, { useState } from "react";
import Header from '../Header/Header'
import styles from '../../styles/Layout.module.css'
import Sidebar from "../Sidebar/Sidebar";
import Grid from '@material-ui/core/Grid'
const Layout = ({ children }) => {
  const [opened, setOpened] = useState(true);
  const handleDrawerToggle = () => {
    setOpened(!opened);
  };
  return (
    <>
      {/* <Meta />
      <Nav /> */}
      <Header 
            logoAltText="QUAT TOOL"
            toggleDrawer={handleDrawerToggle}
          />
      <Grid container spacing={2}>
        {
          opened && (
            <Grid item xs={2}>
            <Sidebar
              opened={opened}
              />
            </Grid>
          )
        }
        
        <Grid item xs={opened?10:12}>
            {children}
        </Grid>
      </Grid>
    </>
  )
}

export default Layout
