import React, { useState } from 'react'
import Link from 'next/link';
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import PropTypes from 'prop-types'
import SidebarItem from './SidebarItem'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import { drawerWidth } from '../../styleVariables'
import { makeStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import withWidth from '@material-ui/core/withWidth'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText';

const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent)

const useStyles = makeStyles(theme => ({
  drawerPaper: {
    position: 'relative',
    width: 'inherit',
    maxWidth: drawerWidth,
    height: '100vh',
    zIndex: theme.zIndex.drawer + 99
  },
  modal: {
    [theme.breakpoints.down('sm')]: {
      top: '56px!important'
    },
    [theme.breakpoints.up('sm')]: {
      top: '64px!important'
    },
    zIndex: '1000!important'
  },
  backdrop: {
    [theme.breakpoints.down('sm')]: {
      top: '56px'
    },
    [theme.breakpoints.up('sm')]: {
      top: '64px'
    }
  }
}))

const Sidebar = ({ opened, toggleDrawer }) => {
  const classes = useStyles()
  const [activeRoute, setActiveRoute] = useState(undefined)
  const toggleMenu = index =>
    setActiveRoute(activeRoute === index ? undefined : index)
  function ListItemLink(props) {
      return <ListItem button component="a" {...props} />;
    }
  const menu = (
    <List component="nav" aria-label="secondary mailbox folders">
      <Link href='/submit-test'>
       <ListItem button style={{borderBottom:"2px solid #3f51b5"}}>
        <ListItemText primary="Create Test" />
        </ListItem>
      </Link>
      <Link href='/test-suite'>
       <ListItem button style={{borderBottom:"2px solid #3f51b5"}}>
        <ListItemText primary="Test Suite" />
        </ListItem>
      </Link>
   </List>
  )

  return (
    <>
      <Hidden smDown>
        <Drawer
          variant='persistent'
          classes={{
            paper: classes.drawerPaper
          }}
          open={opened}
          ModalProps={{
            keepMounted: false,
            className: classes.modal,
            BackdropProps: {
              className: classes.backdrop
            },
            onBackdropClick: toggleDrawer
          }}
        >
          {menu}
        </Drawer>
      </Hidden>
      <Hidden mdUp>
        <SwipeableDrawer
          variant='temporary'
          classes={{
            paper: classes.drawerPaper
          }}
          open={opened}
          onClose={toggleDrawer}
          onOpen={toggleDrawer}
          disableBackdropTransition={!iOS}
          ModalProps={{
            keepMounted: false,
            className: classes.modal,
            BackdropProps: {
              className: classes.backdrop
            },
            onBackdropClick: toggleDrawer
          }}
        >
          {menu}
        </SwipeableDrawer>
      </Hidden>
    </>
  )
}

Sidebar.prototypes = {
  opened: PropTypes.func,
  toggleDrawer: PropTypes.func,
  closeDrawer: PropTypes.func,
  openDrawer: PropTypes.func
  // routes: PropTypes.object
}

const SidebarWithRouter = withRouter(Sidebar)

export default withWidth()(Sidebar)
