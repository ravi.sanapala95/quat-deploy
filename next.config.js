const withPlugins = require('next-compose-plugins');
const withOptimizedImages = require('next-optimized-images');
const withFonts = require('next-fonts');

const nextConfiguration = {
  webpack: (config) => {
    config.externals = config.externals || {};
    return config;
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/login',
        permanent: true,
      },
    ]
  }
};

module.exports = withPlugins(
  [withOptimizedImages, withFonts],
  nextConfiguration
);
